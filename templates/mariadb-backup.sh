#!/bin/bash

backuppath={{ mariadb_backup_destination }}
timestamp=$(date +%Y%b%d_%H%M)
archive_filename=mariadb-${timestamp}.gz
mysqldump_options="--single-transaction --complete-insert --dump-date --all-databases --flush-privileges"

mkdir -p ${backuppath}
if [ ! "/" == "${backuppath}" ]; then
  find ${backuppath}/ -name mariadb-\*.gz -type f -mtime +7 -delete
fi
mysqldump ${mysqldump_options} | gzip > ${backuppath}/${archive_filename}
if [ ! "0" == "$?" ];then
  echo ERROR: mysqldump command failed.
  exit 1
fi
gzip -t ${backuppath}/${archive_filename}
if [ ! "0" == "$?" ];then
  echo ERROR: integrity check failed on compressed backup file.
  exit 2
fi
