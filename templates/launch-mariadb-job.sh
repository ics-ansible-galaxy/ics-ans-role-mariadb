#!/bin/bash
# This is the script that should be uploaded to Elkar Backup. You need to
# replace the below destination variable yourself before uploading it to Elkar.
MARIADB_BACKUP_SCRIPT={{ mariadb_backup_script_location }}

sudo -u elkarbackup ssh ${ELKARBACKUP_SSH_ARGS} ${ELKARBACKUP_URL%:*} ${MARIADB_BACKUP_SCRIPT}
exit $?
